#Khalid Shah
#Email: khalidshah@csus.edu
#This program simply capture picture using webcam and OpenCV libraries

import time
import cv2

#camera port is 0 in most cases
camera_port = 0

#open camera
camera = cv2.VideoCapture(camera_port)

#gap to avoid dark or black images
time.sleep(0.2)

#get the image taken 
return_value, image = camera.read()

#save the image
cv2.imwrite("images.png", image)
print("Image taken")

#close the camera
del(camera) 
