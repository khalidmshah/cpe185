# CPE 185: Lab 6-10 #
This Section is for lab 6-10 for CPE 185 lab class.

## Lab 06: ##

Source Code: lab 6 doesn't have any source code, it was a simple setup through command line.

Wiki Page: [Link](https://bitbucket.org/khalidmshah/cpe185/wiki/Lab%206:%20Take%20Picture%20with%20RPI%20and%20Webcam)

## Lab 07: ##

Source Code: [Link](https://bitbucket.org/khalidmshah/cpe185/src/543c50040410a6810aebdb7e6153f4dabea53d7f/lab7.py?at=master&fileviewer=file-view-default)

Wiki Page: [Link](https://bitbucket.org/khalidmshah/cpe185/wiki/Lab%207:%20Take%20Picture%20with%20RPI%20using%20OpenCV)

## Lab 08: ##
	
Source Code: [Link](https://bitbucket.org/khalidmshah/cpe185/src/543c50040410a6810aebdb7e6153f4dabea53d7f/Lab8.py?at=master&fileviewer=file-view-default)

Wiki Page: [Link](https://bitbucket.org/khalidmshah/cpe185/wiki/Lab%208:%20Servo%20and%20OpenCV%20Webcam)

## Lab 09: ##

Source Code: [Link](https://bitbucket.org/khalidmshah/cpe185/src/543c50040410a6810aebdb7e6153f4dabea53d7f/lab9.py?at=master&fileviewer=file-view-default)

Wiki Page: [Link](https://bitbucket.org/khalidmshah/cpe185/wiki/Lab%209:%20Image%20Processing%20and%20License%20Plate%20Detection)

## Lab 10: ##

Source Code: [Link](https://bitbucket.org/khalidmshah/cpe185/src/543c50040410a6810aebdb7e6153f4dabea53d7f/lab10_client.py?at=master&fileviewer=file-view-default)

Source Code: [Link](https://bitbucket.org/khalidmshah/cpe185/src/543c50040410a6810aebdb7e6153f4dabea53d7f/lab10-server.py?at=master&fileviewer=file-view-default)

Wiki Page: [Link](https://bitbucket.org/khalidmshah/cpe185/wiki/Lab%2010:%20%20File%20transfer%20through%20TCP%20Server%20&%20Client)


###Student ###

Khalid Shah

Email: khalidshah@csus.edu