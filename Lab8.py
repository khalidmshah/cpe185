#Khalid Shah
#Email: khalidshah@csus.edu
#This program is idle program for our final project WPSIO (Which Parking Spot Is Open) robot.
#It will capture image of both sides of the parking lot. Once it takes both images
#it will turn to 90 degrees and will be ready to move forward.

import time
import cv2
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)
#set output pin to 12 for servo
GPIO.setup(12, GPIO.OUT)

#Setting Pulse Width Modulation
p = GPIO.PWM(12, 50)

#starts the servo at 90 degrees
p.start(7.5)

def main():    
    image=""
    counter=0
    
    while counter<20:
        print 'Scanning Parking ', counter
        # sleep for 1 second
        time.sleep(1)
        # turns 180 degrees
        p.ChangeDutyCycle(12.5) 
        image="image"+str(counter)+".png"
        # sleep for 2 seconds
        time.sleep(2) 
        # call camera to take picture
        camera(image)
        counter+=1
        
        print 'Scanning Parking ',counter
        # turns 0 degrees
        p.ChangeDutyCycle(2.5)
        image="image"+str(counter)+".png"
        # sleep for 2 seconds
        time.sleep(2) 
        # call camera to take picture
        camera(image)

        print'Moving Forward...'
        counter+=1
        # turns 90 degrees
        p.ChangeDutyCycle(7.50)
        # sleep for 3 seconds
        time.sleep(3)

#This program simply captures picture using webcam and OpenCV libraries
def camera (images):
    
    #camera port is 0 in most cases
    camera_port = 0

    #open camera
    camera = cv2.VideoCapture(camera_port)

    #sleep to avoid dark or black images
    time.sleep(0.2)

    #get the image taken 
    return_value, image = camera.read()

    #save the image
    cv2.imwrite("images.png", image)
    print("Image taken")

    #close the camera
    del(camera) 

main()