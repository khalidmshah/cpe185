#Khalid Shah
#Email: khalidshah@csus.edu

import time
import cv2

#processes the image and find a license plate and mark it in image. It will return confirmation, and 
#image name if there is license plate detected or not.
def process(img_name, spot_num):
    
    confirm = 0
    #read image
    img = cv2.imread(img_name)
    img_final = cv2.imread(img_name)
    #convert the image to gray scale image
    img2gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #thresholding
    ret, mask = cv2.threshold(img2gray, 180, 255, cv2.THRESH_BINARY)
    image_final = cv2.bitwise_and(img2gray, img2gray, mask=mask)
    ret, new_img = cv2.threshold(image_final, 180, 255, cv2.THRESH_BINARY) 

    #dilate the image to further amplify features
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3)) 
    dilated = cv2.dilate(new_img, kernel, iterations=9)  

    #get contours
    contours, hierarchy = cv2.findContours(dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    [x, y, w, h] = 0,0,0,0

    #finding the right contour
    for contour in contours:
        [x, y, w, h] = cv2.boundingRect(contour)
        if w < 200 or h < 80:
            continue
        if h > 200 or w > 300:
            continue
        if (w/h) < 1.5:
            continue
        
        if x<300 or y<150:
            continue
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 255), 2)
        confirm = 1
        print confirm
        
    img_name = "image"+str(spot_num)+".jpg"
    cv2.imwrite(img_name, img)
    return confirm, img_name

process('Before.jpg', 1)
