#Khalid Shah
#Email: khalidshah@csus.edu

import socket
from socket import *

#This is a client program for server, its job is to send everything
#to the Server through TCP socket connection. It will receive data
#from the main program and will forward it to server. 
image_name='image1.jpg'
confirm=1
spot_num=1


confirm_1 = str(confirm)
spot_num = str(spot_num)
    
HOST = '192.168.1.68'    #server name or IP address 
PORT = 1234            
clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect((HOST,PORT))

#send confirmation 0,1,or 2; Available, Vehicle, Unidentified object.
clientSocket.send(confirm_1.encode())
clientSocket.send(spot_num.encode())

# sends an image only if there is an object in parking spot else it won't send any image
if confirm == 1 or confirm ==2:
    with open(image_name, 'rb') as file_to_send:
        for data in file_to_send:
            clientSocket.sendall(data)
print 'end'
clientSocket.close()
