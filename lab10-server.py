#Khalid Shah
#Email: khalidshah@csus.edu

#This Program gets data from client and save it into Mysql database
import socket
import mysql.connector as mariadb

HOST = '192.168.1.68'                 
PORT = 1234
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.bind((HOST, PORT))


socket.listen(1)
Date="2017-12-7"

while 1:
    
    print 'Ready...'
    conn, addr = socket.accept()
    Ava= conn.recv(1024)
    Ava = Ava.decode()
    print Ava

    Parking_num= conn.recv(1024)
    Parking_num = Parking_num.decode()
    print Parking_num

    LP_Images='/home/pi/Desktop/Khalid/FInal Project/Server/image'+Parking_num+'.jpg'

    #save the image on server side
    with open(LP_Images, 'wb') as file_to_write:
        while True:
            data = conn.recv(1024)
            print data #print for reference 
            if not data:
                break
            file_to_write.write(data)

    #connect the mysql to python program
    mariadb_connection = mariadb.connect(user='root', password='', database='mydb')
    cursor = mariadb_connection.cursor()
    
    #insert information into mysql database 
    try:
            cursor.execute("INSERT INTO wpsio(ID, Parking_num, Ava, LP_Images, Date) VALUES (Null,%s,%s,%s,%s)",\
                           (Parking_num, Ava, LP_Images, Date))
    except mariadb.Error as error:
        print("Error: {}".format(error))

    mariadb_connection.commit()
    print "The last inserted id was: ", cursor.lastrowid

    mariadb_connection.close()

